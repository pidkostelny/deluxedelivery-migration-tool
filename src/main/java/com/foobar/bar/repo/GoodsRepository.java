package com.foobar.bar.repo;

import com.foobar.bar.domain.Goods;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoodsRepository extends JpaRepository<Goods,Long>,JpaSpecificationExecutor<Goods> {

    List<Goods> findAllBySubCategoryId(Long id);

    @Query("select g from Goods g join g.subCategory s join s.category c where c.id=:idCategory")
    List<Goods> findAllByCategory(@Param("idCategory") Long idCategory);

}
