package com.foobar.bar.repo;

import com.foobar.bar.domain.GoodsForOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface GoodsForOrderRepository extends JpaRepository<GoodsForOrder,Long> {

    Set<GoodsForOrder> findByOrderId(Long id);
}
