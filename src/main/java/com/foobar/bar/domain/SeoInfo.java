package com.foobar.bar.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Getter
@Setter
@NoArgsConstructor

@Embeddable
public class SeoInfo {

    private String header;

    private String title;

    @Column(columnDefinition = "text")
    private String description;
}
