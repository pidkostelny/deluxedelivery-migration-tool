package com.foobar.bar.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "sub_category")
public class SubCategory extends IdHolder{

    private String name;

    @Embedded
    private SeoInfo seoInfo;

    @ManyToOne
    private Category category;

    @OneToMany(mappedBy = "subCategory",cascade = CascadeType.REMOVE)
    private Set<Goods> goods = new LinkedHashSet<>();

    @Override
    public String toString() {
        return name;
    }
}
