package com.foobar.migrator;

import com.foobar.bar.repo.GoodsRepository;
import com.foobar.bar.repo.SubCategoryRepository;
import com.foobar.foo.domain.Goods;
import com.foobar.foo.repo.OldGoodsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GoodsMigrator {

    @Autowired
    private OldGoodsRepository oldGoodsRepository;

    @Autowired
    private GoodsRepository goodsRepository;

    @Autowired
    private SubCategoryRepository subCategoryRepository;


    public void migrateGoods(){
        System.out.println("Start to migrate goods");
        List<Goods> oldGoods = oldGoodsRepository.findAll();
        List<com.foobar.bar.domain.Goods> newGoods = oldGoods.stream().map(this::mapToNewGoods).collect(Collectors.toList());
        goodsRepository.saveAll(newGoods);
        System.out.println("End to migrate goods");
    }

    private com.foobar.bar.domain.Goods mapToNewGoods(Goods goods){
        com.foobar.bar.domain.Goods newGoods = new com.foobar.bar.domain.Goods();
        newGoods.setId(goods.getId());
        newGoods.setName(goods.getName());
        newGoods.setDescription(goods.getDescription());
        newGoods.setPathImage(goods.getPathImage());
        newGoods.setPrice(goods.getPrice());
        newGoods.setSubCategory(subCategoryRepository.getOne(goods.getSubCategory().getId()));
        newGoods.setWeight(goods.getWeight());
        System.out.println("Map to new goods\n ID: "+newGoods.getId()+"\nName: "+newGoods.getName());
        return newGoods;
    }

}
