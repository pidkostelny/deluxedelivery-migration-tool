package com.foobar.migrator;

import com.foobar.bar.domain.SeoInfo;
import com.foobar.bar.repo.CategoryRepository;
import com.foobar.foo.domain.Category;
import com.foobar.foo.repo.OldCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CategoryMigrator {

    @Autowired
    private OldCategoryRepository oldOldCategoryRepository;

    @Autowired
    private CategoryRepository newCategoryRepository;

    public void test() {
        List<Category> oldCategories = oldOldCategoryRepository.findAll();
        oldCategories.forEach(e -> System.out.println(e.getName()));

    }

    public void migrateCategory() {
        System.out.println("Start to migrate categories");
        List<Category> oldCategories = oldOldCategoryRepository.findAll();
            List<com.foobar.bar.domain.Category> newCategories = oldCategories.stream().map(this::mapCategoryOldToNew).collect(Collectors.toList());
        newCategoryRepository.saveAll(newCategories);
        System.out.println("End to migrate categories");
    }

    private com.foobar.bar.domain.Category mapCategoryOldToNew(Category category)  {
        com.foobar.bar.domain.Category newCategory = new com.foobar.bar.domain.Category();
        newCategory.setId(category.getId());

        newCategory.setSeoInfo(new SeoInfo());
        newCategory.setName(category.getName());
        System.out.println("Map to new category ->\nId: " + newCategory.getId() + "\nName: " + newCategory.getName() + "\n\n");
        return newCategory;
    }

}
