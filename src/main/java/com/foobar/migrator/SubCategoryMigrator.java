package com.foobar.migrator;

import com.foobar.bar.domain.SeoInfo;
import com.foobar.bar.repo.CategoryRepository;
import com.foobar.bar.repo.SubCategoryRepository;
import com.foobar.foo.domain.SubCategory;
import com.foobar.foo.repo.OldSubCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SubCategoryMigrator {

    @Autowired
    private OldSubCategoryRepository oldSubCategoryRepository;

    @Autowired
    private SubCategoryRepository subCategoryRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Transactional
    public void migrateSubCategory() {
        System.out.println("Start to migrate subCategories");
        List<SubCategory> oldSubCategories = oldSubCategoryRepository.findAll();
        List<com.foobar.bar.domain.SubCategory> subCategories = oldSubCategories.stream().map(this::mapOldToNew).collect(Collectors.toList());
        subCategoryRepository.saveAll(subCategories);
        System.out.println("End to migrate subCategories");
    }


    private com.foobar.bar.domain.SubCategory mapOldToNew(SubCategory subCategory) {
        com.foobar.bar.domain.SubCategory newSubCategory = new com.foobar.bar.domain.SubCategory();
        newSubCategory.setId(subCategory.getId());
        newSubCategory.setName(subCategory.getName());
        newSubCategory.setSeoInfo(new SeoInfo());
        newSubCategory.setCategory(categoryRepository.getOne(subCategory.getCategory().getId()));
        System.out.println("Map to new category \nID: " + newSubCategory.getId() + "\nName: " + newSubCategory.getName());
        return newSubCategory;
    }


}
