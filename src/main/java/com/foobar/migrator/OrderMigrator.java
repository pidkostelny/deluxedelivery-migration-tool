package com.foobar.migrator;

import com.foobar.bar.repo.GoodsForOrderRepository;
import com.foobar.bar.repo.GoodsRepository;
import com.foobar.bar.repo.OrderRepository;
import com.foobar.bar.repo.UserRepository;
import com.foobar.foo.domain.GoodsForOrder;
import com.foobar.foo.domain.Order;
import com.foobar.foo.domain.User;
import com.foobar.foo.repo.OldOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderMigrator {

    @Autowired
    private OldOrderRepository oldOrderRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GoodsRepository goodsRepository;

    @Autowired
    private GoodsForOrderRepository goodsForOrderRepository;

    @Transactional
    public void migrateOrder(){
        System.out.println("Start to migrate orders");
        List<Order> oldOrders = oldOrderRepository.findAll();
        List<com.foobar.bar.domain.Order> newOrders = oldOrders.stream().map(this::mapOrders).collect(Collectors.toList());
        orderRepository.saveAll(newOrders);
        oldOrders.forEach(oldOrder -> oldOrder.getGoodsForOrders().forEach(oldGoodsForOrder -> goodsForOrderRepository.save(mapGoodsForOrder(oldGoodsForOrder, orderRepository.getOne(oldOrder.getId())))));
        oldOrders.forEach(oldOrder -> userRepository.save(mapUser(oldOrder.getUser(), orderRepository.getOne(oldOrder.getId()))));
        System.out.println("End to migrate orders");
    }

    private com.foobar.bar.domain.Order mapOrders(Order oldOrder){
        com.foobar.bar.domain.Order newOrder = new com.foobar.bar.domain.Order();
        newOrder.setId(oldOrder.getId());
        newOrder.setDate(oldOrder.getDate().toLocalDate());
        newOrder.setTime(oldOrder.getDate().toLocalTime());
        newOrder.setDescription(oldOrder.getDescription());
        newOrder.setArchive(oldOrder.getArchive());
        newOrder.setSum(oldOrder.getSum());
//        newOrder.setUser(userRepository.save(mapUser(oldOrder.getUser())));
//        System.out.println("Migrate Order date: "+newOrder.getDate().getDayOfMonth()+":"+newOrder.getDate().getMonth()+":"+newOrder.getDate().getYear());
        return newOrder;
    }

    private com.foobar.bar.domain.User mapUser(User oldUser, com.foobar.bar.domain.Order order) {
        com.foobar.bar.domain.User newUser = new com.foobar.bar.domain.User();
        newUser.setId(oldUser.getId());
        newUser.setName(oldUser.getName());
        newUser.setAddress(oldUser.getAddress());
        newUser.setAddressNumber(oldUser.getAddressNumber());
        newUser.setNumber(oldUser.getNumber());
        newUser.setSurName(oldUser.getSurName());
        newUser.setOrder(order);
        System.out.println("Migrate User with \nName: "+newUser.getName()+"\nSurName: "+newUser.getSurName());
        return newUser;
    }

    private com.foobar.bar.domain.GoodsForOrder mapGoodsForOrder(GoodsForOrder oldGoodsForOrder, com.foobar.bar.domain.Order order){
        com.foobar.bar.domain.GoodsForOrder newGoodsForOrder = new com.foobar.bar.domain.GoodsForOrder();
        newGoodsForOrder.setId(oldGoodsForOrder.getId());
        newGoodsForOrder.setCount(oldGoodsForOrder.getCount());
        newGoodsForOrder.setGoods(goodsRepository.getOne(oldGoodsForOrder.getGoods().getId()));
        newGoodsForOrder.setOrder(order);
        return newGoodsForOrder;
    }

}
