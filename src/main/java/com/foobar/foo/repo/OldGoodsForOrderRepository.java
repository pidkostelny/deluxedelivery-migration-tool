package com.foobar.foo.repo;

import com.foobar.foo.domain.GoodsForOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface OldGoodsForOrderRepository extends JpaRepository<GoodsForOrder,Long> {

    Set<GoodsForOrder> findByOrderId(Long id);
}
