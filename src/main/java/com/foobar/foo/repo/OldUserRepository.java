package com.foobar.foo.repo;

import com.foobar.foo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OldUserRepository extends JpaRepository<User, Long> {
}
