package com.foobar.foo.repo;

import com.foobar.foo.domain.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OldAdministratorRepository extends JpaRepository<Administrator,Long> {

    Administrator findByLogin(String login);
}
