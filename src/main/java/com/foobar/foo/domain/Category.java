package com.foobar.foo.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter @Setter
@Entity
@Table(name = "category")
public class Category extends IdHolder{

    @Column(unique = true)
    private String name;

    @OneToMany(mappedBy = "category",cascade = CascadeType.REMOVE)
    private Set<SubCategory> subCategoryList = new LinkedHashSet<>();

    @Override
    public String toString() {
        return name;
    }
}
