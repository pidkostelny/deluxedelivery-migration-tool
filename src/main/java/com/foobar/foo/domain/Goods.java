package com.foobar.foo.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

@Getter @Setter
@Entity
@Table(name = "goods")
public class Goods extends IdHolder{

    private String name;

    private Long price;

    private Long weight;

    private String description;

    @Column(name = "path_image")
    private String pathImage;

    @ManyToOne
    @JoinColumn(name="sub_category_id")
    private SubCategory subCategory;

    @OneToMany(mappedBy = "goods",cascade = CascadeType.REMOVE)
    private Set<GoodsForOrder> goodsForOrders = new LinkedHashSet<>();




}
