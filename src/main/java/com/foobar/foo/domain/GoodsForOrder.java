package com.foobar.foo.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter @Setter
@Entity
@Table(name =  "goods_for_order")
public class GoodsForOrder extends IdHolder{

    private Long count;

    @ManyToOne
    private Goods goods;

    @ManyToOne
    private Order order;

}
