package com.foobar.foo.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter @Setter
@Entity
@Table(name = "_user")
public class User extends IdHolder {

    private String name;

    @Column(name = "sur_name")
    private String surName;

    private String number;

    private String address;

    @Column(name = "address_number")
    private String addressNumber;

    @OneToOne
    @MapsId
    private Order order;



}
