package com.foobar;

import com.foobar.migrator.CategoryMigrator;
import com.foobar.migrator.GoodsMigrator;
import com.foobar.migrator.OrderMigrator;
import com.foobar.migrator.SubCategoryMigrator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class Application {

	@Autowired
	private CategoryMigrator categoryMigrator;

	@Autowired
	private SubCategoryMigrator subCategoryMigrator;

	@Autowired
	private GoodsMigrator goodsMigrator;

	@Autowired
	private OrderMigrator orderMigrator;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@PostConstruct
	public void postConstruct(){
//		categoryMigrator.test();
		categoryMigrator.migrateCategory();
		subCategoryMigrator.migrateSubCategory();
		goodsMigrator.migrateGoods();
		orderMigrator.migrateOrder();
	}
}
